import { Component, Input, OnInit } from "@angular/core";
import { ProductService } from "./product.service";
import { proveedor, productos } from "./products.model";
import { Router } from "@angular/router";
import { AlertController } from "@ionic/angular";
import { NavController, ToastController } from "@ionic/angular";

@Component({
  selector: "page-products",
  templateUrl: "./products.page.html",
  styleUrls: ["./products.page.scss"],
})
export class ProductsPage implements OnInit {
  @Input() nombre: string;
  products: productos[];
  constructor(
    private productServices: ProductService,
    private router: Router,
    private alertController: AlertController,
    public navCtrl: NavController,
    public toastCtrl: ToastController
  ) {}

  ngOnInit() {
  //  console.log("Carga inicial"); //test

  //load initial data
    this.products = this.productServices.getAll();
  }

  // data load before being loaded
  ionViewWillEnter() {
    console.log("Se obtuvo la lista");
    this.products = this.productServices.getAll();
  }

  //details products
  view(code: number) {
    this.router.navigate(["/products/detail/" + code]);
  }

  //delete product by id
  delete(code: number) {
    this.alertController
      .create({
        header: "Borrar Producto",
        message: "Esta seguro que desea borrar este producto?",
        buttons: [
          {
            text: "No",
            role: "no",
          },
          {
            text: "Borrar",
            handler: () => {
              this.productServices.deleteProduct(code);
              this.products = this.productServices.getAll();
            },
          },
        ],
      })
      .then((alertEl) => {
        alertEl.present();
      });
  }

  //edit product by id
  update(code: number) {
    this.router.navigate(["/products/edit/" + code]);
  }

  //message to logout
  async logout() {
    this.navCtrl.navigateRoot("/");

    const toast = await this.toastCtrl.create({
      message: "Cierre Correcto.",
      duration: 2000,
      cssClass: "toast-message",
    });
    toast.present();
  }
}
