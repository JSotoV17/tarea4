import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { ProductService } from "../product.service";

@Component({
  selector: "app-add",
  templateUrl: "./add.page.html",
  styleUrls: ["./add.page.scss"],
})
export class AddPage implements OnInit {
  formProdutAdd: FormGroup;
  constructor(private serviceProduct: ProductService, private router: Router) {}

  ngOnInit() {

    //validations inputs

    this.formProdutAdd = new FormGroup({
      pcantidad: new FormControl(1, {
        updateOn: "blur",
        validators: [Validators.required, Validators.min(1)],
      }),
      pcodigo: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(3)],
      }),
      pnombre: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(3)],
      }),
      ppeso: new FormControl(1, {
        updateOn: "blur",
        validators: [Validators.required, Validators.min(1)],
      }),
      pfecha_caducidad: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pdescripcion: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(20)],
      }),
    });
  }

 

  addProduct() { //add product to array

    if (!this.formProdutAdd.valid) {
      return;
    }

    this.serviceProduct.addProduct(
      this.formProdutAdd.value.pprecio,
      this.formProdutAdd.value.pcantidad,
      this.formProdutAdd.value.pcodigo,
      this.formProdutAdd.value.pnombre,
      this.formProdutAdd.value.ppeso,
      this.formProdutAdd.value.pfecha_caducidad,
      this.formProdutAdd.value.pdescripcion
    );

    
    this.formProdutAdd.reset();  //clean form
    
    this.router.navigate(["/products"]);
    
  }
}
