import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AlertController } from "@ionic/angular";
import { ProductService } from "../product.service";
import { productos } from "../products.model";

@Component({
  selector: "app-detail",
  templateUrl: "./detail.page.html",
  styleUrls: ["./detail.page.scss"],
})
export class DetailPage implements OnInit {
  product: productos;
  constructor(
    private activeRouter: ActivatedRoute,
    private productService: ProductService,
    private router: Router,
    private alertController: AlertController
  ) {}

  ngOnInit() {
    //obtain the parameter to search the product and then the index
    this.activeRouter.paramMap.subscribe((paramMap) => {
      if (!paramMap.has("productId")) {
        return;
      }
      const productId = parseInt(paramMap.get("productId"));
      this.product = this.productService.getProduct(productId);
      // console.log(productId); -> Test
    });
  }

  //delete prodcut to array

  deleteProduct() {
    this.alertController
      .create({
        header: "Borrar Producto?",
        message: "realmente desea borrar el producto?",
        buttons: [
          {
            text: "No",
            role: "no",
          },
          {
            text: "Borrar",
            handler: () => {
              this.productService.deleteProduct(this.product.codigo);
              this.router.navigate(["./products"]);
            },
          },
        ],
      })
      .then((alertEl) => {
        alertEl.present();
      });
  }
}
