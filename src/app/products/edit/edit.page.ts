import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ProductService } from "../product.service";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { productos } from "../products.model";

@Component({
  selector: "app-edit",
  templateUrl: "./edit.page.html",
  styleUrls: ["./edit.page.scss"],
})
export class EditPage implements OnInit {
  product: productos;
  formProductEdit: FormGroup;

  constructor(
    private activeRouter: ActivatedRoute,
    private serviceProduct: ProductService,
    private router: Router
  ) {}

  ngOnInit() {
    //obtain the parameter to search the product and then the index

    this.activeRouter.paramMap.subscribe((paramMap) => {
      if (!paramMap.has("productId")) {
        return;
      }
      const productId = parseInt(paramMap.get("productId"));
      this.product = this.serviceProduct.getProduct(productId);
    });

    //validations inputs

    this.formProductEdit = new FormGroup({
      pcantidad: new FormControl(this.product.candidad, {
        updateOn: "blur",
        validators: [Validators.required, Validators.min(1)],
      }),
      pcodigo: new FormControl(this.product.codigo, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(3)],
      }),
      pnombre: new FormControl(this.product.nombre, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(3)],
      }),
      ppeso: new FormControl(this.product.peso, {
        updateOn: "blur",
        validators: [Validators.required, Validators.min(1)],
      }),
      pfecha_caducidad: new FormControl(this.product.fecha_caducidad, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pdescripcion: new FormControl(this.product.descripcion, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(20)],
      }),
    });

    this.formProductEdit.value.pnombre = this.product.nombre;
  }

  editProduct() {
    //edit product to array

    if (!this.formProductEdit.valid) {
      return;
    }

    this.serviceProduct.editProduct(
      this.formProductEdit.value.pprecio,
      this.formProductEdit.value.pcantidad,
      this.formProductEdit.value.pcodigo,
      this.formProductEdit.value.pnombre,
      this.formProductEdit.value.ppeso,
      this.formProductEdit.value.pfecha_caducidad,
      this.formProductEdit.value.pdescripcion
    );

    this.formProductEdit.reset(); //clean form

    this.router.navigate(["./products"]);
  }
}
