import { compileNgModuleFromRender2 } from "@angular/compiler/src/render3/r3_module_compiler";
import { Injectable } from "@angular/core";
import { productos, proveedor } from "./products.model";

@Injectable({
  providedIn: "root",
})
export class ProductService {

  //array provider
  private proveedore: proveedor[] = [
    {
      nombre: "Intcomex",
      cedula: 12345,
      direccion: "San Jose",
      numero: 20009964,
      correo: "info@intcomex.com",
      codigo: 2,
    },
    {
      nombre: "EuroComp",
      cedula: 67890,
      direccion: "Heredia",
      numero: 20009965,
      correo: "info@eurocomp.com",
      codigo: 1,
    },
  ];

//array products
  private productos: productos[] = [
    {
      proveedor: this.proveedore,
      precio: 500,
      candidad: 10,
      codigo: 1,
      nombre: "Tarjeta Madre 1151",
      peso: 100,
      fecha_caducidad: "11-11-1",
      descripcion: "Tarjeta Madre 1151",
    },
    {
      proveedor: this.proveedore,
      precio: 100,
      candidad: 20,
      codigo: 2,
      nombre: "SSD NVME",
      peso: 200,
      fecha_caducidad: "11-11-1",
      descripcion: "SSD NVME",
    },
  ];
  constructor() {}

  // get all elements to array
  getAll() {
    return [...this.productos];
  }

  //get elements for ID

  getProduct(productId: number) {
    return {
      ...this.productos.find((product) => {
        return product.codigo === productId;
      }),
    };
  }

//delete elements to array by number

  deleteProduct(productId: number) {
    this.productos = this.productos.filter((product) => {
      return product.codigo !== productId;
    });
  }


  //add products to array

  addProduct(
    pprecio: number,
    pcantidad: number,
    pcodigo: number,
    pnombre: string,
    ppeso: number,
    pfecha_caducidad: string,
    pdescripcion: string
  ) {
    const product: productos = {
      proveedor: this.proveedore,
      precio: pprecio,
      candidad: pcantidad,
      codigo: pcodigo,
      nombre: pnombre,
      peso: ppeso,
      fecha_caducidad: pfecha_caducidad,
      descripcion: pdescripcion,
    };
    this.productos.push(product);
  }

  //edit products, according to the index of the selected element

  editProduct(
    pprecio: number,
    pcantidad: number,
    pcodigo: number,
    pnombre: string,
    ppeso: number,
    pfecha_caducidad: string,
    pdescripcion: string
  ) {
    let index = this.productos.map((x) => x.codigo).indexOf(pcodigo); //get the index of the element

    this.productos[index].codigo = pcodigo;
    this.productos[index].candidad = pcantidad;
    this.productos[index].nombre = pnombre;
    this.productos[index].peso = ppeso;
    this.productos[index].fecha_caducidad = pfecha_caducidad;
    this.productos[index].descripcion = pdescripcion;
    this.productos[index].precio = pprecio;

    //second option

    /* let myObj = this.productos.find(ob => ob.codigo = pcodigo);

    myObj.candidad= pcantidad;
    myObj.precio= pprecio;
    myObj.nombre= pnombre;
    myObj.peso= ppeso;
    myObj.descripcion= pdescripcion;
    myObj.fecha_caducidad= pfecha_caducidad;

    this.productos[index] = myObj;*/

    //console.log(this.productos);
  }
}
